/**
  * Created by askellio on 10/18/17.
  *
  */


object Main extends App{

  import scala.io.StdIn._

  println()
  println("lab1")
  println()

  val p = pascal(_,_)

  val tag = "!!!!!!!!!!!!!!1!!"
  println()
  println ("task#1 "+tag)
  println()
  println(p.apply(2, 4))
  println(p.apply(1, 1))
  println(pascal(0, 4))
  println(pascal(1, 4))

  println()
  println ("task#2 "+tag)
  println()
  println(balance("(if (zero? x) max (/ 1 x))".toList))
  println(balance("((x) (y) ((z)+k)-f)".toList))
  println(balance(")(".toList))
  println(balance("(()".toList))

  println()
  println ("task#3 "+tag)
  println()
  println(countChange(4, List(1, 2).sorted))
  println(countChange(12, List(10, 1, 3)))

  def pascal (column: Int, row: Int): Int = {
    if (column<0 || column>row)
      0
    else {
      if (row == 0)
        1
      else
        pascal(column-1, row-1) + pascal(column, row-1)
    }
  }

  def balance(list: List[Char]): Boolean = {
    _balance(0, 0, list)
  }

  def _balance (opened: Int, closed: Int, list: List[Char]): Boolean = {
    if (list.isEmpty)
      opened == closed
    else
      if (opened < closed)
        false
      else
        if (list.head == '(')
          _balance(opened+1, closed, list.tail)
        else
          if (list.head == ')')
            _balance(opened, closed+1, list.tail)
          else
            _balance(opened, closed, list.tail)
  }

  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0)
      1
    else if (money < 0 || coins.isEmpty)
      0
    else
      countChange(money-coins.head, coins)+countChange(money, coins.tail)
  }
}
